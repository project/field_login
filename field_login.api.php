<?php

/**
 * @file
 * Hooks specific to the field login module.
 */

/**
 * The default value cannot be used as a definition for login credentials.
 *
 * @param string $field_name
 *   Implement the field name for user login.
 * @param string $username
 *   The value passed in by the user login form.
 *
 * @return array
 *   UID for loading user entity data.
 */
function hook_field_login(string $field_name, string $username): array {
  if ($field_name == 'field_telephone') {
    return \Drupal::entityQuery('user')
      ->accessCheck()
      ->condition('field_telephone.local_number', $username)
      ->execute();
  }
  return [];
}
