<?php

namespace Drupal\field_login\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Set the list of allowed login fields.
 */
class FieldLoginSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configFactory);
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'field_login.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'field_login_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $settings = $this->config('field_login.settings');

    // Exclude default field list.
    $exclude_default_types = [
      'image',
      'datetime',
      'list_string',
      'string_long',
      'text_long',
      'text',
      'boolean',
      'timestamp',
      'list_integer',
      'decimal',
      'float',
    ];

    $exclude_types = array_flip($exclude_default_types);

    // Exclude default field list.
    $exclude_default_fields = [
      'uuid',
      'langcode',
      'preferred_langcode',
      'preferred_admin_langcode',
      'pass',
      'timezone',
      'status',
      'created',
      'changed',
      'access',
      'login',
      'init',
      'roles',
      'default_langcode',
    ];

    $exclude_fields = array_flip($exclude_default_fields);
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');

    $field_options = [];
    foreach ($fields as $field) {
      if (isset($exclude_fields[$field->getName()]) || isset($exclude_types[$field->getType()])) {
        continue;
      }

      $field_options[$field->getName()] = $field->getLabel();
    }

    $form['fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Login Configurations'),
      '#open' => TRUE,
    ];

    $form['fields']['login_field'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Login field'),
      '#default_value' => $settings->get('login_field'),
      '#description' => $this->t('Select the fields that allow users to log in.'),
      '#options' => $field_options,
      '#required' => TRUE,
    ];

    $form['fields']['override_login_labels'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override login form'),
      '#default_value' => $settings->get('override_login_labels'),
      '#description' => $this->t('This option allows you to override the login form username title/description.'),
    ];

    $form['fields']['login_username_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Override label'),
      '#default_value' => $settings->get('login_username_title'),
      '#states' => [
        'visible' => [
          ':input[name="override_login_labels"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Override the username field title.'),
    ];

    $form['fields']['login_username_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Override description'),
      '#default_value' => $settings->get('login_username_description'),
      '#states' => [
        'visible' => [
          ':input[name="override_login_labels"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Override the username field description.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (empty($form_state->getValue('login_field'))) {
      $form_state->setErrorByName('login_field', $this->t("Please select at least one login field."));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $settings = $this->config('field_login.settings');

    foreach ($form_state->getValues() as $field_name => $field_value) {
      if ($field_name === 'login_field') {
        $settings->set($field_name, array_filter($field_value));
      }
      else {
        $settings->set($field_name, $field_value);
      }
    }

    $settings->save();
  }

}
