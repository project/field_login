<?php

namespace Drupal\field_login;

/**
 * Provide an interface for user authentication services.
 */
interface UserVerificationInterface {

  /**
   * Verify if the user exists.
   *
   * @param string $identifier
   *   The value of the login field passed in by the user.
   */
  public function validate(string $identifier);

}
