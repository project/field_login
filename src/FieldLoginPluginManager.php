<?php

namespace Drupal\field_login;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * FieldLogin plugin manager.
 */
class FieldLoginPluginManager extends DefaultPluginManager implements FieldLoginPluginManagerInterface {

  /**
   * Constructs FieldLoginPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/FieldLogin',
      $namespaces,
      $module_handler,
      'Drupal\field_login\FieldLoginInterface',
      'Drupal\field_login\Annotation\FieldLogin'
    );
    $this->setCacheBackend($cache_backend, 'field_login_plugins');
    $this->alterInfo('field_login_info');
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldLoginPlugin(string $field_name): bool {
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    foreach ($this->getDefinitions() as $definition) {
      if ($definition['field_name'] == $field_name && !empty($definition['field_type']) &&
        $fields[$field_name]->getType() == $definition['field_type']
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLoginPlugin(string $field_name, string $username): array {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($this->hasFieldLoginPlugin($field_name)) {
        $instances = $this->createInstance($plugin_id);
        return $instances->getAccountUid($username);
      }
    }
    return [];
  }

}
