<?php

namespace Drupal\field_login;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a user verification service.
 */
class UserVerification implements UserVerificationInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\field_login\FieldLoginPluginManagerInterface
   */
  protected FieldLoginPluginManagerInterface $fieldLoginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Constructs a User Verification object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\field_login\FieldLoginPluginManagerInterface $field_login_plugin_manager
   *   The field login plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * The messenger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, FieldLoginPluginManagerInterface $field_login_plugin_manager, ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->fieldLoginManager = $field_login_plugin_manager;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(string $identifier) {
    $username = trim($identifier);
    $login_fields = $this->configFactory->get('field_login.settings')->get('login_field');
    $user_storage = $this->entityTypeManager->getStorage('user');

    $account = FALSE;
    // Hook Query User ID.
    $hook = 'field_login';
    $uid = '';
    foreach ($login_fields as $field) {
      $loginName = $field === 'mail' ? filter_var($username, FILTER_VALIDATE_EMAIL) : $username;

      // Plugin Query User ID.
      if (!empty($this->fieldLoginManager->hasFieldLoginPlugin($field))) {
        $uid = $this->fieldLoginManager->getFieldLoginPlugin($field, $username);
      }

      // HOOK Query User ID.
      if ($this->moduleHandler->hasImplementations($hook) &&
        $hook_uid = $this->moduleHandler->invokeAll($hook, [
          $field,
          $username,
        ])) {
        $uid = $hook_uid;
      }

      // Query User ID.
      if ($query_uid = $user_storage->getQuery()
        ->accessCheck(FALSE)
        ->condition($field, $loginName)
        ->execute()) {
        $uid = $query_uid;
      }

      // Load User.
      if (!empty($uid) && $entities = $user_storage->loadMultiple($uid)) {
        /** @var \Drupal\user\UserInterface $user */
        $user = reset($entities);

        if ($user->isActive()) {
          $account = $user;
        }
      }
    }

    return $account;
  }

}
