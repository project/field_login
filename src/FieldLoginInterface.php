<?php

namespace Drupal\field_login;

/**
 * Interface for field_login plugins.
 */
interface FieldLoginInterface {

  /**
   * Get User UID.
   *
   * @param string $username
   *   The value passed in by the user login form.
   */
  public function getAccountUid(string $username): array;

}
