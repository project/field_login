<?php

namespace Drupal\field_login;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for field_login plugins.
 */
abstract class FieldLoginPluginBase extends PluginBase implements FieldLoginInterface {

  /**
   * {@inheritdoc}
   */
  public function getAccountUid($username): array {
    return [];
  }

}
