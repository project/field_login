<?php

namespace Drupal\field_login;

use Drupal\Core\Password\PasswordInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserAuthenticationInterface;

/**
 * Validates user authentication credentials.
 */
class UserAuthDecorator implements UserAuthenticationInterface {

  /**
   * The original user authentication service.
   *
   * @var \Drupal\user\UserAuthenticationInterface
   */
  protected UserAuthenticationInterface $userAuth;

  /**
   * The password hashing service.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected PasswordInterface $passwordChecker;

  /**
   * The user storage.
   *
   * @var \Drupal\field_login\UserVerificationInterface
   */
  protected UserVerificationInterface $userVerification;

  /**
   * Constructs a UserAuth object.
   *
   * @param \Drupal\user\UserAuthenticationInterface $user_auth
   *   The original user authentication service.
   * @param \Drupal\field_login\UserVerificationInterface $user_verification
   *   This provide user authentication services.
   */
  public function __construct(UserAuthenticationInterface $user_auth, PasswordInterface $password_checker, UserVerificationInterface $user_verification) {
    $this->userAuth = $user_auth;
    $this->passwordChecker = $password_checker;
    $this->userVerification = $user_verification;
  }

  /**
   * {@inheritdoc}
   */
  public function lookupAccount($identifier): UserInterface|false {
    if (!empty($identifier)) {
      if ($account = $this->userVerification->validate($identifier)) {
        return $account;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateAccount(UserInterface $account, #[\SensitiveParameter] string $password): bool {
    if ($this->passwordChecker->check($password, $account->getPassword())) {
      // Update user to new password scheme if needed.
      if ($this->passwordChecker->needsRehash($account->getPassword())) {
        $account->setPassword($password);
        $account->save();
      }
      return TRUE;
    }
    return FALSE;
  }

}
