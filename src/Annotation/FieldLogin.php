<?php

namespace Drupal\field_login\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines field_login annotation object.
 *
 * @Annotation
 */
class FieldLogin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The description of the plugin.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public string $description;

  /**
   * Field name for user login.
   *
   * @var string
   */
  public string $field_name;

  /**
   * Field type for user login.
   *
   * @var string
   */
  public string $field_type;

}
