<?php

namespace Drupal\field_login;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * The interface of FieldLogin plugin manager.
 */
interface FieldLoginPluginManagerInterface extends PluginManagerInterface {

  /**
   * Determine if the plugin exists.
   *
   * @param string $field_name
   *   Implement the field name for user login.
   *
   * @return bool
   *   Determine if the plugin exists.
   */
  public function hasFieldLoginPlugin(string $field_name): bool;

  /**
   * Get the user's UID from the plugin.
   *
   * @param string $field_name
   *   Implement the field name for user login.
   * @param string $username
   *   The value of the login field passed in by the user.
   *
   * @return array
   *   UID for loading user entity data.
   */
  public function getFieldLoginPlugin(string $field_name, string $username): array;

}
