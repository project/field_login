## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

- This module allows users to log in using field data values, administrators can
  customize the fields allowed to log in.
- Special fields can use plug-ins or hooks to query the user UID to achieve
  login, for example: Phone number (field). You can also use any data to log in,
  you only need to query and output the user's UID.

#### For a full description of the module visit:

https://www.drupal.org/project/field_login

#### To submit bug reports and feature suggestions, or to track changes visit:

https://www.drupal.org/project/issues/field_login

## REQUIREMENTS

- Field values must be unique
- Field values cannot have special symbols or spaces

## INSTALLATION

Install the field login module as you would normally install a contributed
Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION

Go to "/admin/config/people/accounts/field-login" for the configuration screen,
available configuraitons:

- Select login field address: This option enables the user to login to the field
  address
- Override login form: This option allows you to override the login form
  username title/description.
- Login form username title: Override the username field title.
- Login form username description: Override the username field description.

## HOOK FIELD LOGIN

#### Example: Phone number (field)

```
function hook_field_login(string $field_name, $username): array {
  if ($field_name == 'field_telephone') {
    return \Drupal::entityQuery('user')
      ->accessCheck()
      ->condition('field_telephone.local_number', $username)
      ->execute();
  }
  return [];
}
```

## PLUGIN FIELD LOGIN

#### Example: Phone number (field)

Please place the file in your own module: src\Plugin\FieldLogin

```
use Drupal\field_login\FieldLoginPluginBase;

/**
 * Plugin implementation of the field_login.
 *
 * @FieldLogin(
 *   id = "phone",
 *   label = @Translation("Phone field"),
 *   description = @Translation("Phone field description."),
 *   field_name="field_phone",
 *   field_type="sms_phone_number"
 * )
 */
class PhoneField extends FieldLoginPluginBase {

  public function getAccountUid($username): array {
    return \Drupal::entityQuery('user')
      ->accessCheck()
      ->condition('field_phone.local_number', $username)
      ->execute();
  }

}
```

## MAINTAINERS

This module was created by gaoxiang, a drupal developer.

- Gao Xiang - https://www.drupal.org/u/qiutuo
